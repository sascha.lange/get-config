package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"sync"

	"github.com/schollz/progressbar"
	ccsv "github.com/tsak/concurrent-csv-writer"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/configservice"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/organizations"
	"github.com/aws/aws-sdk-go/service/sts"
)

var csvHeader = []string{
	"Account",
	"Region",
	"Config Rule Name",
	"Config Rule Identifier",
	"Config Rule State",
	"Config Rule Owner",
	"Recorder configured",
	"Recorder enabled",
	"Record all ressources",
	"Recorder include global ressources",
}

// Default aws region for api calls that need a region
const constRegion = "eu-west-1"

const (
	constRuleName       = 0
	constRuleIdentifier = 1
	constRuleState      = 2
	constRuleOwner      = 3
)

const (
	constConfigured      = "configured"
	constEnabled         = "enabled"
	constRecordAllRes    = "recordAllRes"
	constRecordGlobalRes = "recordGlobalRes"
)

func main() {
	// Check command line flags
	assumeRoleArg := flag.String("role", "OrganizationAccountAccessRole", "AWS assumeRole name")
	regionArg := flag.String("region", "", "AWS region name")
	accountArg := flag.String("account", "", "AWS account id")
	blacklistArg := flag.String("blacklist", "", "Provide blacklist name")
	outputfileArg := flag.String("outpout", "result.csv", "Changes the name of the created csv file")
	profileArg := flag.String("profile", "", "AWS profile name for authentication")

	flag.Parse()

	// Create and validate session
	sess := createSession(*profileArg)
	_, err := sess.Config.Credentials.Get()
	checkError("Error reading credentials\n", err)

	myBlacklist := readBlacklist(*blacklistArg)
	checkError("Error reading blacklist\n", err)

	myAccounts := createAccountList(sess, *accountArg)
	fmt.Printf("Number of discovered sub accounts: %d\n", len(myAccounts)-1)

	myRegions := createRegionList(sess, *regionArg)
	fmt.Printf("Number of discovered regions: %d\n", len(myRegions))

	fmt.Println("Start processing ...")
	bar := progressbar.New(len(myRegions) * len(myAccounts))

	currentAccountID := getCurrentAccountID(sess)

	var wg sync.WaitGroup
	wg.Add(len(myAccounts))

	csv, err := ccsv.NewCsvWriter(*outputfileArg)
	checkError("Cannot create output file\n", err)
	defer csv.Close()
	csv.Write(csvHeader)

	// Prepare error.log
	errLog, err := os.Create("error.log")
	checkError("Cannot create error.log", err)
	defer errLog.Close()
	var errMu sync.Mutex

	// Global error state
	isErr := false

	for _, account := range myAccounts {
		bar.Add(1)
		if accountBlacklisted(account, myBlacklist) {
			wg.Done()
			continue
		}
		go func(account string, csv *ccsv.CsvWriter) {
			defer wg.Done()
			for _, region := range myRegions {
				bar.Add(1)
				newSess := assumeRoleOnSubAccount(sess, currentAccountID, account, *assumeRoleArg, region)
				_, err := newSess.Config.Credentials.Get()
				if err != nil {
					isErr = true
					accessErr := fmt.Sprintf("Cannot login to account %s in region %s\n", account, region)
					message := fmt.Sprintf("%s%v\n\n", accessErr, err.Error())
					writeErrorLog(message, errLog, &errMu)
					continue
				}

				mRec := getConfigRecorder(newSess)
				sConfigRules := getConfigRules(newSess)
				for _, configRule := range sConfigRules {
					csv.Write([]string{
						account,
						region,
						configRule[constRuleName],
						configRule[constRuleIdentifier],
						configRule[constRuleState],
						configRule[constRuleOwner],
						mRec[constConfigured],
						mRec[constEnabled],
						mRec[constRecordAllRes],
						mRec[constRecordGlobalRes],
					})
				}
			}
		}(account, csv)
	}
	wg.Wait()
	errorInfo(isErr)
}

func errorInfo(isErr bool) {
	if isErr {
		fmt.Printf("\nErrors during run! Check your error.log\n")
	}
}

func writeErrorLog(m string, f *os.File, w *sync.Mutex) {
	w.Lock()
	defer w.Unlock()

	f.WriteString(m)
	f.Sync()
}

func assumeRoleOnSubAccount(sess *session.Session, currentAcc string, acc string, role string, reg string) *session.Session {
	var newSess *session.Session
	if currentAcc != acc {
		roleToAssumeArn := fmt.Sprintf("arn:aws:iam::%v:role/%v", acc, role)
		creds := stscreds.NewCredentials(sess, roleToAssumeArn)
		newSess = sess.Copy(&aws.Config{
			Region:      aws.String(reg),
			Credentials: creds,
		})
	} else {
		newSess = sess.Copy(&aws.Config{
			Region: aws.String(reg),
		})
	}
	return newSess
}

func createRegionList(sess *session.Session, region string) []string {
	var regions []string
	if len(region) <= 0 {
		regions = getRegions(sess)
		return regions
	}
	return []string{region}
}

func createSession(profile string) *session.Session {
	var sess *session.Session
	if len(profile) <= 0 {
		sess = session.Must(session.NewSession(&aws.Config{
			Region: aws.String(constRegion),
		}))
		return sess
	}
	sess = session.Must(session.NewSessionWithOptions(session.Options{
		Config:  aws.Config{Region: aws.String(constRegion)},
		Profile: profile,
	}))
	return sess
}

func getConfigRecorder(sess *session.Session) map[string]string {
	configSvc := configservice.New(sess)
	configRecorders, err := configSvc.DescribeConfigurationRecorders(&configservice.DescribeConfigurationRecordersInput{})
	checkError("Cannot get aws config recorders\n", err)

	configRecorderStatus, err := configSvc.DescribeConfigurationRecorderStatus(&configservice.DescribeConfigurationRecorderStatusInput{})
	checkError("Cannot get aws config recorder status\n", err)

	recorderInfos := make(map[string]string)

	if len(configRecorderStatus.ConfigurationRecordersStatus) <= 0 {
		recorderInfos[constConfigured] = "-"
		recorderInfos[constEnabled] = "-"
		recorderInfos[constRecordAllRes] = "-"
		recorderInfos[constRecordGlobalRes] = "-"
	} else {
		recorderInfos[constConfigured] = "yes"
		recorderInfos[constEnabled] = strconv.FormatBool(*configRecorderStatus.ConfigurationRecordersStatus[0].Recording)
		recorderInfos[constRecordAllRes] = strconv.FormatBool(*configRecorders.ConfigurationRecorders[0].RecordingGroup.AllSupported)
		recorderInfos[constRecordGlobalRes] = strconv.FormatBool(*configRecorders.ConfigurationRecorders[0].RecordingGroup.IncludeGlobalResourceTypes)
	}
	return recorderInfos
}

func createAccountList(sess *session.Session, account string) []string {
	if len(account) <= 0 {
		return getOrganizationAccounts(sess)
	}
	return []string{account}
}

func getConfigRules(sess *session.Session) [][]string {
	configSvc := configservice.New(sess)
	configRules, err := configSvc.DescribeConfigRules(&configservice.DescribeConfigRulesInput{})
	checkError("Cannot get aws config rules\n", err)
	token := configRules.NextToken
	var rules [][]string

	if len(configRules.ConfigRules) != 0 {
		for _, configRule := range configRules.ConfigRules {
			rules = append(rules, []string{*configRule.ConfigRuleName, *configRule.Source.SourceIdentifier, *configRule.ConfigRuleState, *configRule.Source.Owner})
		}

		for token != nil {
			moreConfigRules, _ := configSvc.DescribeConfigRules(&configservice.DescribeConfigRulesInput{
				NextToken: token,
			})
			token = moreConfigRules.NextToken
			for _, configRule := range moreConfigRules.ConfigRules {
				rules = append(rules, []string{*configRule.ConfigRuleName, *configRule.Source.SourceIdentifier, *configRule.ConfigRuleState, *configRule.Source.Owner})
			}
		}
	} else {
		rules = append(rules, []string{"-", "-", "-", "-"})
	}
	return rules
}

func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}

func accountBlacklisted(account string, blacklist []string) bool {
	var result bool
	for _, element := range blacklist {
		if element == account {
			result = true
			break
		}
	}
	return result
}

func readBlacklist(path string) []string {
	var lines []string
	if len(path) != 0 {
		file, err := os.Open(path)
		checkError("Cannot open blacklist\n", err)
		defer file.Close()
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			lines = append(lines, scanner.Text())
		}
	}
	return lines
}

func getRegions(sess *session.Session) []string {
	ec2Svc := ec2.New(sess)
	regions, err := ec2Svc.DescribeRegions(&ec2.DescribeRegionsInput{})
	checkError("Cannot get regions\n", err)
	var result []string
	for _, region := range regions.Regions {
		result = append(result, *region.RegionName)
	}
	return result
}

func getCurrentAccountID(sess *session.Session) string {
	stsSvc := sts.New(sess)
	idendtity, err := stsSvc.GetCallerIdentity(&sts.GetCallerIdentityInput{})
	checkError("Cannot get main account details\n", err)
	return *idendtity.Account
}

func getOrganizationAccounts(sess *session.Session) []string {
	orgSvc := organizations.New(sess)
	orgAccounts, err := orgSvc.ListAccounts(&organizations.ListAccountsInput{})
	checkError("Cannot get organization accounts\n", err)
	token := orgAccounts.NextToken
	var accounts []string
	for _, account := range orgAccounts.Accounts {
		accounts = append(accounts, *account.Id)
	}

	for token != nil {
		moreOrgAccounts, err := orgSvc.ListAccounts(&organizations.ListAccountsInput{
			NextToken: token,
		})
		checkError("Cannot fetch additional organization accounts\n", err)
		token = moreOrgAccounts.NextToken
		for _, account := range moreOrgAccounts.Accounts {
			accounts = append(accounts, *account.Id)
		}
	}
	return accounts
}
