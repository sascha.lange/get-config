# Changelog

## [v0.6.0] - 2019-08-05
## Added
- Show config rule owner

## [v0.5.0] - 2019-08-02
### Fix
- Fix region switch for main account

## [v0.4.0] - 2019-08-02
### Added
- Get aws config also from main account
- Add pagination support to get all results from aws configservice api

### Changed
- Refactored code for readability

## [v0.3.0] - 2019-07-31
### Added
- Write number of regions to stdout
- Write number of sub accounts to stdout
- Write progress bar to stdout

### Changed
- getTrails supports pagination to get all results from the aws organizations api
